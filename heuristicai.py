import operator
import random
import game
import sys
import math
import numpy

# Author:				chrn (original by nneonneo)
# Date:				11.11.2016
# Description:			The logic of the AI to beat the game.

UP, DOWN, LEFT, RIGHT = 0, 1, 2, 3

DIRECTIONS = [
    UP, DOWN, LEFT, RIGHT
]


def find_best_move(board: numpy.ndarray, move_number: int):
    agent = checks_potential_neighbours_agent

    return agent(board, move_number)


global state


def checks_potential_neighbours_agent(board: numpy.ndarray, *args):
    """
    Calculates the following for each possible move:
     - Amount of merges happening
     - Sum of log2(tile-Size) of all merges
     - Amount of theoretically possible moves after this move (not guaranteed since new tiles get added)
      - Amount and Sum of merges for these theoretical moves
    """
    moves = get_available_moves(board)

    def get_scores(board_to_score: numpy.ndarray, moves_avail: list):
        move_scores = {}

        for move in moves_avail:
            merge_cnt = 0
            merge_size = 0
            noop_cnt = 0
            for i in range(4):
                score = move_score_available_in_row_index(board_to_score, move, i)
                if score > 0:
                    merge_cnt += 1
                    merge_size += score
                else:
                    noop_cnt += 1

            move_scores[move] = {
                'cnt': merge_cnt,
                'size': merge_size,
                'noop': noop_cnt,
            }
        return move_scores

    scores_now = get_scores(board, moves)

    scores_future = {}

    for move in moves:
        new_board = execute_move(move, board)
        new_moves = get_available_moves(new_board)
        scores_future[move] = get_scores(new_board, new_moves)

    # Adapt these params to find a good solution:
    move_weights = {
        DOWN: 1.15,
        RIGHT: 1.05,
        LEFT: 1.04,
        UP: 1,
    }

    weight_count = 0.01
    weight_size = 0.012
    weight_noop = 0

    weight_ft_mv_count = 0.1 * 0.005
    weight_ft_count = 0.1 * 0.01
    weight_ft_size = 0.1 * 0.05
    weight_ft_noop = 0

    weight_ft_move_type = {
        DOWN: 1.5,
        RIGHT: 1.45,
        LEFT: 1.1,
        UP: 1,
    }

    weight_ft_move = 0.01

    for move, score in scores_now.items():
        move_weights[move] += weight_count * score['cnt']
        move_weights[move] += weight_size * score['size']
        move_weights[move] += weight_noop * score['noop']

        # Add future weights
        move_weights[move] += weight_ft_mv_count * len(scores_future[move].keys())
        for ft_move, i_score in scores_future[move].items():
            move_weights[move] += weight_ft_count * i_score['cnt']
            move_weights[move] += weight_ft_size * i_score['size']
            move_weights[move] += weight_ft_noop * i_score['noop']

            move_weights[move] += weight_ft_move * weight_ft_move_type[ft_move]

    directions = sorted(move_weights.items(), key=operator.itemgetter(1))
    directions = reversed(directions)

    for direction in directions:
        if direction[0] in moves:
            return direction[0]

    return -1


def execute_move(move, board):
    """
    move and return the grid without a new random tile 
    It won't affect the state of the game in the browser.
    """

    if move == UP:
        return game.merge_up(board)
    elif move == DOWN:
        return game.merge_down(board)
    elif move == LEFT:
        return game.merge_left(board)
    elif move == RIGHT:
        return game.merge_right(board)
    else:
        sys.exit("No valid move")


"""
A dictionary which contains a function for each direction.
Pass the board and index to get the numbers specific for that row/column
"""
ROW_AND_COLUMN_FUNCTIONS = {
    UP: lambda board, i: numpy.flip(board[:, i]),
    DOWN: lambda board, i: board[:, i],
    LEFT: lambda board, i: numpy.flip(board[i, :]),
    RIGHT: lambda board, i: board[i, :],
}


def get_available_moves(board: numpy.ndarray):
    """
    Returns a list which includes a subset of [UP,DOWN,LEFT,RIGHT] which only includes the moves that
    are currently possible
    """
    moves = []

    for key, list_callback in ROW_AND_COLUMN_FUNCTIONS.items():
        for idx in range(4):
            row = list_callback(board, idx)
            if move_available_in_row(row):
                moves.append(key)
                break

    return moves


def move_available_in_row(row):
    """
    Determines if a move is possible through this row
    Returns true if any non-0-value is superseded by a 0-value
    Returns true if any two cells have neighbouring cells (ignoring any gaps which would imply the first rule anyways)
    Returns false otherwise
    """
    has_cell = False
    last_neighbour = 0
    for i in range(row.size):
        if row[i] > 0:
            if last_neighbour == row[i]:
                # Last item was the same, can be combined
                return True

            has_cell = True
            last_neighbour = row[i]
        else:
            if has_cell:
                # Cell available but empty space, can be moved into
                return True
    return False


TYPE_NONE, TYPE_MOVE, TYPE_COMBINE = 0, 1, 2


def move_score_available_in_row_index(board: numpy.ndarray, direction: int, index: int):
    return move_score_available_in_row(ROW_AND_COLUMN_FUNCTIONS[direction](board, index))


def move_score_available_in_row(row):
    """
    Determines if a move is available and if that move results in a combination or just a move of cells
    Returns -1 if no move is available
    Returns 0 if it will only move cells but not combine
    Returns log2(cell-value) of the largest combination of cells
    """
    move_size = -1
    has_cell = False
    last_neighbour = 0
    for i in range(row.size):
        if row[i] > 0:
            if last_neighbour == row[i]:
                # Last item was the same, can be combined
                move_size = max(move_size, row[i])

            has_cell = True
            last_neighbour = row[i]
        else:
            if has_cell:
                # Cell available but empty space, can be moved into
                move_size = max(move_size, 0)

    if move_size > 0:
        return math.log2(move_size)
    return move_size


def board_equals(board: numpy.ndarray, newboard: numpy.ndarray):
    """
    Check if two boards are equal
    """
    return (newboard == board).all()
