import random

import numpy as np

from ai.data import Board, RIGHT, LEFT, DOWN, UP, F_MOVES, R, F_VALUES, L, F_MERGE_COUNT, F_MERGES


class ConsoleControl:
    STATUS_RUNNING = "running"
    STATUS_WON = "won"
    STATUS_ENDED = "ended"

    def __init__(self, lut):
        self.lut = lut
        self.status = self.STATUS_RUNNING
        self.start_cells = 2
        self.board = None
        self.score = 0

        self.init_board()

    def init_board(self):
        self.score = 0
        matrix = np.zeros((4, 4,), dtype=np.uint)
        self.board = Board.from_matrix(self.lut, matrix)
        for i in range(self.start_cells):
            self._add_random_cell()

    def _add_random_cell(self):
        e = self.board.empty_count()
        if e == 0:
            return

        matrix = self.board.get_as_matrix()
        val = 1 if random.random() <= 0.9 else 2

        e = random.choice(range(e))

        for k in range(4 * 4):
            x = int(k / 4)
            y = int(k % 4)

            if matrix[x][y] == 0:
                if e == 0:
                    matrix[x][y] = val
                    break
                else:
                    e -= 1

        self.board = Board.from_matrix(self.lut, matrix)

    def setup(self):
        pass

    def get_status(self) -> str:
        """
        Returns the status of the current game
        :return: "running" | "won" | "ended"
        """
        for i in range(4):
            n = self.board.move(i)
            if n != self.board:
                return self.STATUS_RUNNING
        return self.STATUS_ENDED

    def get_score(self) -> int:
        """
        :return:
        """
        return self.score

    def get_board(self) -> np.ndarray:
        """
        :return: Current 4x4 board
        """
        return 2 ** self.board.get_as_matrix(dtype=np.uint64)

    def restart_game(self):
        """
        Reset the board
        """
        self.init_board()
        pass

    def continue_game(self):
        """
        Continue game when won
        """
        pass

    def execute_move(self, direction: int):
        """
        :param move: 0 UP, 1 RIGHT, 2 LEFT, 3 DOWN
        """
        new_matrix = np.empty((4, 4,), dtype=np.uint8)
        if direction == RIGHT:
            for i in range(4):
                for k in range(self.board.rows[i][F_MOVES][R][F_MERGE_COUNT]):
                    self.score += 2 ** self.board.rows[i][F_MOVES][R][F_MERGES][k]
                new_matrix[i] = self.board.rows[i][F_MOVES][R][F_VALUES]
        elif direction == LEFT:
            for i in range(4):
                for k in range(self.board.rows[i][F_MOVES][L][F_MERGE_COUNT]):
                    self.score += 2 ** self.board.rows[i][F_MOVES][L][F_MERGES][k]
                new_matrix[i] = self.board.rows[i][F_MOVES][L][F_VALUES]
        elif direction == DOWN:
            for i in range(4):
                for k in range(self.board.cols[i][F_MOVES][R][F_MERGE_COUNT]):
                    self.score += 2 ** self.board.cols[i][F_MOVES][R][F_MERGES][k]
                new_matrix[i] = self.board.cols[i][F_MOVES][R][F_VALUES]
            new_matrix = np.transpose(new_matrix)
        elif direction == UP:
            for i in range(4):
                for k in range(self.board.cols[i][F_MOVES][L][F_MERGE_COUNT]):
                    self.score += 2 ** self.board.cols[i][F_MOVES][L][F_MERGES][k]
                new_matrix[i] = self.board.cols[i][F_MOVES][L][F_VALUES]
            new_matrix = np.transpose(new_matrix)
        self.board = Board.from_matrix(self.lut, new_matrix)
        self._add_random_cell()
