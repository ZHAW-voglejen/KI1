#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author:      chrn (original by nneonneo)
# Date:        11.11.2016, updated by scik 30.08.2019
# Copyright:   https://github.com/nneonneo/2048-ai
# Description: Helps the user achieve a high score in a real game of 2048 by using a move searcher.
#              This Script initialize the AI and controls the game flow.


import time

from pandas import DataFrame

from ai.data import convert_web_board_matrix, Board, RowLUT
from ai.expectimax import ExpectiMax
from consolectrl import ConsoleControl

global LUT


def get_ai():
    """
    Setup and configure AI
    """
    ai = ExpectiMax(LUT)
    return ai


def print_board(m):
    for row in m:
        for c in row:
            print('%8d' % c, end=' ')
        print()


def _to_val(c):
    if c == 0:
        return 0
    return c


def to_val(m):
    return [[_to_val(c) for c in row] for row in m]


def _to_score(c):
    if c <= 1:
        return 0
    return (c - 1) * (2 ** c)


def to_score(m):
    return [[_to_score(c) for c in row] for row in m]


def movename(move):
    return ['up', 'down', 'left', 'right'][move]


def movename_c(move):
    return ['u', 'd', 'l', 'r'][move]


def play_game(gamectrl):
    ai = get_ai()

    moveno = 0
    start = time.time()
    while 1:
        state = gamectrl.get_status()
        if state == 'ended':
            break
        elif state == 'won':
            time.sleep(0.75)
            gamectrl.continue_game()

        moveno += 1
        board = gamectrl.get_board()
        move = ai.get_next_move(Board.from_matrix(ai.lut, convert_web_board_matrix(board)))
        if move < 0:
            break
        print(movename_c(move), end="")
        # print("%010.6f: Score %d, Move %d: %s" % (time.time() - start, gamectrl.get_score(), moveno, movename(move)))
        gamectrl.execute_move(move)

    score = gamectrl.get_score()
    board = gamectrl.get_board()
    maxval = max(max(row) for row in to_val(board))
    print("")
    print("Game over. Final score %d; highest tile %d." % (score, maxval))
    #print(Board.from_matrix(LUT, convert_web_board_matrix(board)).format())
    runtime = time.time() - start
    return {
        'score': score,
        'max': maxval,
        'time': runtime,
        'moves': moveno
    }


def show_results(results, stat_file=None):
    df = DataFrame.from_records(results)
    print(df)
    if stat_file:
        df.to_csv(stat_file, encoding="utf-8", index=False)



def parse_args(argv):
    import argparse

    parser = argparse.ArgumentParser(description="Use the AI to play 2048 via browser control")
    parser.add_argument('-p', '--port', help="Port number to control on (default: 32000 for Firefox, 9222 for Chrome)",
                        type=int)
    parser.add_argument('-b', '--browser',
                        help="Browser you're using. Only Firefox with the Remote Control extension, and Chrome with "
                             "remote debugging (default), are supported right now.",
                        default='chrome', choices=('firefox', 'chrome'))
    parser.add_argument('-k', '--ctrlmode',
                        help="Control mode to use. If the browser control doesn't seem to work, try changing this.",
                        default='hybrid', choices=('keyboard', 'fast', 'hybrid', 'console'))
    parser.add_argument('-r', '--repeat',
                        help="Repeat n times", type=int)
    parser.add_argument('-t', '--timeout',
                        help="Abort after t seconds", type=int)
    parser.add_argument('-s', '--stats',
                        help="Output stats to file", type=str, default=None)

    return parser.parse_args(argv)


def main(argv):
    args = parse_args(argv)

    global LUT
    LUT = RowLUT()
    LUT.load()

    if args.ctrlmode == 'console':
        gamectrl = ConsoleControl(LUT)
    else:
        if args.browser == 'firefox':
            from ffctrl import FirefoxRemoteControl
            if args.port is None:
                args.port = 32000
            ctrl = FirefoxRemoteControl(args.port)
        elif args.browser == 'chrome':
            from chromectrl import ChromeDebuggerControl
            if args.port is None:
                args.port = 9222
            ctrl = ChromeDebuggerControl(args.port)
        else:
            raise Exception('No browser arg provided!')

        if args.ctrlmode == 'keyboard':
            from gamectrl import Keyboard2048Control
            gamectrl = Keyboard2048Control(ctrl)
        elif args.ctrlmode == 'fast':
            from gamectrl import Fast2048Control
            gamectrl = Fast2048Control(ctrl)
        elif args.ctrlmode == 'hybrid':
            from gamectrl import Hybrid2048Control
            gamectrl = Hybrid2048Control(ctrl)
        else:
            raise Exception('Invalid control mode!')

    if gamectrl.get_status() == 'ended':
        gamectrl.restart_game()

    if args.timeout:
        results = []
        start = time.time()
        while time.time() - start < args.timeout:
            results.append(play_game(gamectrl))
            gamectrl.restart_game()

        show_results(results, args.stats)
    elif args.repeat:
        results = []
        for i in range(args.repeat):
            results.append(play_game(gamectrl))
            gamectrl.restart_game()

        show_results(results, args.stats)
    else:
        play_game(gamectrl)


if __name__ == '__main__':
    import sys

    exit(main(sys.argv[1:]))
