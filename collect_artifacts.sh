#!/bin/bash

PROJECT="20982100"
BASE="https://gitlab.com/api/v4"

if [ -z ${1+z} ]; then

  data=`http --session gitlab "$BASE/projects/$PROJECT/pipelines" | jq ".[].id"`
  echo "Choose pipeline"
  echo $data

  exit 0
fi

PIPELINE=$1
mkdir -p "./data/$PIPELINE"

jobs=`http --session gitlab "$BASE/projects/$PROJECT/pipelines/$PIPELINE/jobs" | jq ".[].id"`

for job in $jobs; do
  echo "Downloading $job"
  http --session gitlab "$BASE/projects/$PROJECT/jobs/$job/artifacts/results-$job.yml" > "./data/$PIPELINE/results-$job.csv"
done