import math

import numpy as np

UP, DOWN, LEFT, RIGHT = 0, 1, 2, 3
DIRECTIONS = [UP, DOWN, LEFT, RIGHT]

MAX_VARIANT = 2048 * 4
B = 4
WEIGHT_MATRIX = [
    [B ** 0, B ** 1, B ** 2, B ** 3],
    [B ** 7, B ** 6, B ** 5, B ** 4],
    [B ** 8, B ** 9, B ** 10, B ** 11],
    [B ** 15, B ** 14, B ** 13, B ** 12],
]

# WEIGHT_MATRIX = [
#     [B * 0, B * 1, B * 2, B * 3],
#     [B * 7, B * 6, B * 5, B * 4],
#     [B * 8, B * 9, B * 10, B * 11],
#     [B * 15, B * 14, B * 13, B * 12],
# ]

E = 2

WEIGHT_EMPTY = [
    [E ** 15, E ** 14, E ** 13, E ** 12],
    [E ** 8, E ** 9, E ** 10, E ** 10],
    [E ** 7, E ** 6, E ** 5, E ** 4],
    [E ** 0, E ** 1, E ** 2, E ** 3]
]

# WEIGHT_EMPTY = [
#     [E * 15, E * 14, E * 13, E * 12],
#     [E * 8, E * 9, E * 10, E * 10],
#     [E * 7, E * 6, E * 5, E * 4],
#     [E * 0, E * 1, E * 2, E * 3]
# ]

MAX_EMPTY_WEIGHT = np.sum(WEIGHT_EMPTY)
MAX_EMPTY_WEIGHT_F = 1 / MAX_EMPTY_WEIGHT

F_HAS_DATA = 'hd'
F_VALUES = 'v'

F_PROB = 'p'
F_POSSIBLE_MOVES = 'mp'
L, R = 1, 0

F_MERGE_COUNT = 'mc'
F_MERGES = 'mr'

F_PREDICTION_COUNT = 'pc'
F_PREDICTION_PROB_SUM = 'prps'

F_EMPTY_COUNT = 'ecnt'

F_VALUE_SCORE = 'vscr'
F_EMPTY_SCORE = 'escr'

F_MOVES = 'm'
MOVE_TYPE = np.dtype([
    (F_VALUES, np.uint8, (4,)),
    (F_MERGE_COUNT, np.uint8),
    (F_MERGES, np.uint8, (2,)),
])

F_PREDICTIONS = 'pr'
PREDICTION_TYPE = np.dtype([
    (F_PROB, np.uint8),  # Probability in perdecage, 4 cell -> 1, 2 cell -> 9
    (F_VALUES, np.uint8, (4,)),
])

ENTRY_TYPE = np.dtype([
    (F_VALUES, np.uint8, (4,)),
    (F_POSSIBLE_MOVES, np.bool_, (2,)),  # 0 -> M_RIGHT, 1 -> M_LEFT
    (F_MOVES, MOVE_TYPE, (2,)),  # 0,i > M_RIGHT, 1,i > M_LEFT
    (F_PREDICTION_COUNT, np.uint8),
    (F_PREDICTIONS, PREDICTION_TYPE, (8,)),  # F_PREDICTION_COUNT amount of entries

    (F_EMPTY_COUNT, np.uint8),

    # Assume entry is row n, F_VALUE_SCORE[n] is sum(cell * WEIGHT_MATRIX[n])
    (F_VALUE_SCORE, np.uint64, (4,)),

    # Assume entry is row n, F_EMPTY_SCORE[n] is sum(if cell[i] != 0: WEIGHT_MATRIX[n])
    (F_EMPTY_SCORE, np.uint64, (4,)),
])


class RowLUT:

    def __init__(self):
        variant_count = int(math.log2(MAX_VARIANT) + 1)
        self.do_generate = True
        self.predict_4 = True
        self.variant_count = variant_count
        self.entries = np.zeros((variant_count, variant_count, variant_count, variant_count),
                                dtype=ENTRY_TYPE)

    def save(self):
        np.save("rowlut.npy", self.entries)

    def load(self):
        self.entries = np.load("rowlut.npy")

    def generate_cache(self):
        print("Generating LUT")
        if not self.do_generate:
            print("Generation skipped")
            return

        v = self.variant_count
        for n1 in range(v):
            for n2 in range(v):
                for n3 in range(v):
                    for n4 in range(v):
                        self.generate((n1, n2, n3, n4))
        print("Done")

    def get_arr(self, row: np.ndarray):
        if not self.do_generate:
            return self.gen_arr(row)
        return self.entries[row[0], row[1], row[2], row[3]]

    def gen_arr(self, row: np.ndarray):
        return self.generate((row[0], row[1], row[2], row[3]))

    def generate(self, row: tuple):
        n1, n2, n3, n4 = row
        entry = self.entries[n1, n2, n3, n4]

        # Add aliases
        values = entry[F_VALUES]
        move_r = entry[F_MOVES][R]
        move_r_merges = move_r[F_MERGES]
        move_r_values = move_r[F_VALUES]

        move_l = entry[F_MOVES][L]
        move_l_values = move_l[F_VALUES]
        move_l_merges = move_l[F_MERGES]

        value_scores = entry[F_VALUE_SCORE]
        empty_scores = entry[F_EMPTY_SCORE]

        count_empty = 0
        count_filled = 0
        move_r_merge_count = 0
        move_l_merge_count = 0
        cell_sum = 0

        # Calculate entry data

        fw_val_last = 0
        rv_val_last = 0

        move_possible_f = False
        move_possible_r = False

        fw_offset_mov = 0
        rv_offset_mov = 0

        # Fill Data
        for fw_i in range(4):
            rv_i = 3 - fw_i
            fw_cell_val = row[fw_i]
            rv_cell_val = row[rv_i]

            for j in range(4):
                value_scores[j] += WEIGHT_MATRIX[j][fw_i] * fw_cell_val

            if fw_cell_val == 0:
                for j in range(4):
                    empty_scores[j] += WEIGHT_EMPTY[j][fw_i]

            # Fill values list
            values[fw_i] = fw_cell_val
            cell_sum += fw_cell_val

            if rv_cell_val == 0:
                # Increase offset for every empty cell to move number ahead
                fw_offset_mov += 1

                if rv_val_last > 0:
                    move_possible_r = True
            else:
                if rv_val_last == rv_cell_val:  # Merge happening
                    fw_offset_mov += 1
                    move_r_values[rv_i + fw_offset_mov] = rv_cell_val + 1
                    rv_val_last = 0

                    move_r_merges[move_r_merge_count] = rv_cell_val + 1
                    move_r_merge_count += 1

                    move_possible_r = True
                else:
                    move_r_values[rv_i + fw_offset_mov] = rv_cell_val
                    rv_val_last = rv_cell_val

            if fw_cell_val == 0:
                rv_offset_mov += 1
                count_empty += 1

                if fw_val_last > 0:
                    move_possible_f = True

            else:
                count_filled += 1

                if fw_val_last == fw_cell_val:  # Merge happening
                    rv_offset_mov += 1
                    move_l_values[fw_i - rv_offset_mov] = fw_cell_val + 1
                    fw_val_last = 0

                    move_l_merges[move_l_merge_count] = fw_cell_val
                    move_l_merge_count += 1

                    move_possible_f = True
                else:
                    move_l_values[fw_i - rv_offset_mov] = fw_cell_val
                    fw_val_last = fw_cell_val

        move_l[F_MERGE_COUNT] = move_l_merge_count
        move_r[F_MERGE_COUNT] = move_r_merge_count

        entry[F_EMPTY_COUNT] = count_empty
        entry[F_POSSIBLE_MOVES][R] = move_possible_f
        entry[F_POSSIBLE_MOVES][L] = move_possible_r

        prediction_count = count_empty * 2 if self.predict_4 else count_empty

        # Calculate predictions
        for i in range(prediction_count):  # Fill prediction table
            for j in range(4):
                entry[F_PREDICTIONS][i][F_VALUES][j] = entry[F_VALUES][j]

        # Insert predictions
        p_cnt = 0
        prob_sum = 0
        for i in range(4):
            if values[i] == 0:
                if self.predict_4:
                    k = p_cnt * 2
                    entry[F_PREDICTIONS][k][F_PROB] = 9
                    entry[F_PREDICTIONS][k][F_VALUES][i] = 1
                    prob_sum += 9

                    entry[F_PREDICTIONS][k + 1][F_PROB] = 1
                    entry[F_PREDICTIONS][k + 1][F_VALUES][i] = 2
                    prob_sum += 1
                else:
                    entry[F_PREDICTIONS][p_cnt][F_PROB] = 9
                    entry[F_PREDICTIONS][p_cnt][F_VALUES][i] = 1
                    prob_sum += 9
                p_cnt += 1

        entry[F_PREDICTION_COUNT] = prediction_count

        return entry

    @staticmethod
    def _has_flag(entry, flag):
        return entry['flags'] & flag > 0

    @staticmethod
    def _enable_flag(entry, flag):
        entry['flags'] = entry['flags'] | flag


class Board:

    @staticmethod
    def from_matrix(row_cache: RowLUT, board_state):
        rows = np.empty((4,), dtype=ENTRY_TYPE)
        cols = np.empty((4,), dtype=ENTRY_TYPE)

        for i in range(4):
            rows[i] = row_cache.get_arr(board_state[i, :])
            cols[i] = row_cache.get_arr(board_state[:, i])

        return Board(row_cache, rows, cols)

    def __init__(self,
                 lut: RowLUT,
                 rows: np.ndarray,
                 cols: np.ndarray):
        self.lut = lut
        self.rows = rows
        self.cols = cols

    def hash_depth(self, depth) -> int:
        r0, r1, r2, r3 = self.rows[0], self.rows[1], self.rows[2], self.rows[3]
        return hash(
            (depth,
             r0[F_VALUES][0], r0[F_VALUES][1], r0[F_VALUES][2], r0[F_VALUES][3],
             r1[F_VALUES][0], r1[F_VALUES][1], r1[F_VALUES][2], r1[F_VALUES][3],
             r2[F_VALUES][0], r2[F_VALUES][1], r2[F_VALUES][2], r2[F_VALUES][3],
             r3[F_VALUES][0], r3[F_VALUES][1], r3[F_VALUES][2], r3[F_VALUES][3])
        )

    def get_as_matrix(self, dtype=np.uint8):
        matrix = np.empty((4, 4), dtype=dtype)
        for x in range(4):
            for y in range(4):
                matrix[x, y] = self.rows[x][F_VALUES][y]

        return matrix

    def move(self, direction):
        new_matrix = np.empty((4, 4,), np.uint8)
        if direction == RIGHT:
            for i in range(4):
                new_matrix[i] = self.rows[i][F_MOVES][R][F_VALUES]
        elif direction == LEFT:
            for i in range(4):
                new_matrix[i] = self.rows[i][F_MOVES][L][F_VALUES]
        elif direction == DOWN:
            for i in range(4):
                new_matrix[i] = self.cols[i][F_MOVES][R][F_VALUES]
            new_matrix = np.transpose(new_matrix)
        elif direction == UP:
            for i in range(4):
                new_matrix[i] = self.cols[i][F_MOVES][L][F_VALUES]
            new_matrix = np.transpose(new_matrix)
        else:
            raise IndexError("Invalid direction", direction)

        return self.from_matrix(self.lut, new_matrix)

    def value_score(self):
        return np.sum([self.rows[i][F_VALUE_SCORE][i] for i in range(4)])

    def empty_score(self):
        return np.sum([self.rows[i][F_EMPTY_SCORE][i] for i in range(4)])

    def empty_count(self):
        return np.sum([row[F_EMPTY_COUNT] for row in self.rows])

    def __eq__(self, other):
        return (self.rows[:][F_VALUES] == other.rows[:][F_VALUES]).all()

    def format(self, pow2=True):
        str = "=============================\n"
        for row in self.rows:
            for cell in row[F_VALUES]:
                if cell == 0:
                    str += "|      "
                else:
                    str += "| %4d " % (2 ** cell)
            str += "|\n"
        str += "=============================\n"
        return str


def convert_web_board_matrix(board_matrix: np.ndarray):
    matrix = np.empty((4, 4), dtype=np.uint8)

    for x in range(4):
        for y in range(4):
            val = board_matrix[x, y]
            val = 0 if val == 0 else int(math.log2(val))
            matrix[x, y] = val

    return matrix
