from ai.data import *


class ExpectiMax:

    def __init__(self, lut=None):
        if lut is not None:
            self.lut = lut
        else:
            self.lut = RowLUT()
            self.lut.generate_cache()

        # Score cache for pruning
        self.score_cache = {}

        self.print = False
        self.p_move = False
        self.depth = {
            0: 7,
            1: 6,
            2: 6,
            3: 6,
            4: 6,
            5: 5,
            6: 5,
            7: 5,
            8: 5,

            -1: 3
        }

    def get_next_move(self, board: Board):
        self.score_cache = {}

        scores = [self.evaluate_move(board, move) for move in DIRECTIONS]

        if self.print:
            print(board.format())
            print("E-S", board.empty_score())
            print("Up:    ", scores[UP])
            print("Down:  ", scores[DOWN])
            print("Left:  ", scores[LEFT])
            print("Right: ", scores[RIGHT])

        bestmove = scores.index(max(scores))
        return bestmove

    def evaluate_move(self, board: Board, move):
        n_board = board.move(move)

        if n_board == board:
            return -1

        cnt = n_board.empty_count()
        if cnt in self.depth:
            depth = self.depth[cnt]
        else:
            depth = self.depth[-1]

        print(depth, end='')

        return self.expectimax(n_board, depth)

    def expectimax(self, board: Board, depth, player_move=False):
        hash = board.hash_depth(depth)

        if hash in self.score_cache:
            return self.score_cache[hash]

        if depth == 0:
            return self.heuristic(board)

        if player_move:
            # Player Move
            moves = [self.expectimax(board, depth - 1, False) for board in self.player_moves(board)]
            if len(moves) == 0:
                return 0
            v = max(moves)
            self.score_cache[hash] = v
            return v

        else:
            # Chance move
            moves = self.chance_move(board)
            if len(moves) == 0:
                return self.expectimax(board, depth - 1, True)

            tot = 0
            p_tot = 0
            for p, new_board in moves:
                tot += p * self.expectimax(new_board, depth - 1, True)
                p_tot += p

            v = tot / p_tot
            self.score_cache[hash] = v
            return v

    def heuristic(self, board: Board, p=False):
        f = board.value_score()

        if p:
            print("H-F", f)

        return f

    def player_moves(self, board: Board):
        moved_boards = []
        for direction in DIRECTIONS:
            new_board = board.move(direction)
            if not new_board.__eq__(board):
                moved_boards.append(new_board)

        return moved_boards

    def chance_move(self, board: Board):
        boards = []
        for x in range(4):
            for y in range(board.rows[x][F_PREDICTION_COUNT]):
                matrix = board.get_as_matrix()
                pred = board.rows[x][F_PREDICTIONS][y]
                matrix[x] = pred[F_VALUES]
                boards.append((pred[F_PROB], Board.from_matrix(self.lut, matrix)))

        return boards
