import math
import random
from abc import abstractmethod, ABC

import game
import sys
import numpy

# Author:      chrn (original by nneonneo)
# Date:        11.11.2016
# Copyright:   Algorithm from https://github.com/nneonneo/2048-ai
# Description: The logic to beat the game. Based on expectimax algorithm.

UP, DOWN, LEFT, RIGHT = 0, 1, 2, 3
MOVES = range(4)

MOVE_NAMES = {
    UP: "UP   ",
    DOWN: "DOWN ",
    LEFT: "LEFT ",
    RIGHT: "RIGHT"
}

def find_best_move(board, *args):
    """
    find the best move for the next turn.
    """

    result = [score_toplevel_move(i, board) for i in MOVES]
    best_move = result.index(max(result))

    for m in MOVES:
        print("move: %s score: %.4f" % (MOVE_NAMES[m], result[m]))

    return best_move


MAX_DEPTH = 3


def score_toplevel_move(move, board, depth=0):
    """
    Entry Point to score the first move.
    """
    newboard = execute_move(move, board)

    # Move not possible
    if board_equals(board, newboard):
        return 0

    if depth < MAX_DEPTH:
        new_boards = simulate_cell_add_multiple(newboard, add_amount=1)
        simulated_scores = []

        for new_board in new_boards:
            scores = [score_toplevel_move(m, new_board, depth + 1) for m in MOVES]

            move_weights = {
                DOWN: 1,
                RIGHT: 0.95,
                LEFT: 0.9,
                UP: 0.3
            }

            score = max(scores) * move_weights[move]
            simulated_scores.append(score)

        return numpy.average(simulated_scores)

    return sum_tile_score_times_empty(newboard)



def default_cell_addition_callback(board, **kwargs):
    return [board]


def default_board_scoring_callback(**kwargs):
    return 0


def default_intermediate_score_modification(scores, **kwargs):
    return scores


def score_move(move, board,
               cell_addition_callback=default_cell_addition_callback,
               board_scoring_callback=default_board_scoring_callback,
               max_move_recursions=0,
               intermediate_score_modification=default_intermediate_score_modification,
               depth=0, ):
    if depth < max_move_recursions:
        pass


def sum_tile_score_times_empty(board):
    empty = 0
    cell_sum = 0
    for row in board:
        for cell in row:
            if cell == 0:
                empty += 1
            else:
                cell_sum += math.log2(cell) * 3
    return cell_sum * empty


def sum_tile_score(board):
    max_value = 0
    for row in board:
        for cell in row:
            max_value += cell
    print(max_value)
    return max_value


def max_tile_score(board):
    max_value = 0
    for row in board:
        for cell in row:
            max_value = max(max_value, cell)
    print(max_value)
    return max_value


def simulate_cell_add_multiple(board, add_amount=1, probability_4=0.1):
    empty_positions = get_empty_positions(board)
    if len(empty_positions) == 0:
        return [board]

    choices = random.choices(empty_positions, k=add_amount)

    boards = []
    for choice in choices:
        cell_value = (4 if random.random() <= probability_4 else 2)
        new_board = copy_board(board)
        new_board[choice[0], choice[1]] = cell_value
        boards.append(new_board)

    return boards


def simulate_cell_add(board):
    return simulate_cell_add_multiple(board, add_amount=1)[0]


def get_empty_positions(board):
    empty_positions = []
    for x in range(len(board)):
        for y in range(len(board[x])):
            if board[x, y] == 0:
                empty_positions.append((x, y))
    return empty_positions


def copy_board(board):
    return numpy.array(board)


def execute_move(move, board):
    """
    move and return the grid without a new random tile 
    It won't affect the state of the game in the browser.
    """

    if move == UP:
        return game.merge_up(board)
    elif move == DOWN:
        return game.merge_down(board)
    elif move == LEFT:
        return game.merge_left(board)
    elif move == RIGHT:
        return game.merge_right(board)
    else:
        sys.exit("No valid move")


def board_equals(board, newboard):
    """
    Check if two boards are equal
    """
    return (newboard == board).all()
