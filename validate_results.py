from os import listdir
from os import path
from os.path import isdir

import pandas


def get_files(dir):
    paths = []
    files = listdir(dir)
    for file in files:
        file = path.join(dir, file)
        if isdir(file):
            for k in get_files(file):
                paths.append(k)
        else:
            paths.append(file)
    return paths


def read_data() -> pandas.DataFrame:
    PATH = "./data/"

    dirs = listdir(PATH)

    data = pandas.DataFrame()

    for file in get_files(PATH):
        if "results.csv" in file:
            continue
        c = pandas.read_csv(file)
        if c.size > 0:
            data = data.append(c)
    return data


if __name__ == '__main__':
    data = read_data()
    data["time_per_move"] = data["time"] / data["moves"]
    data = data.rename(
        columns={"score": "Score", "max": "Tile Value", "time": "Time until death", "moves": "Move Count",
                 "time_per_move": "s per Move"})

    print(data)

    data.to_csv("./data/results.csv", index=False)

    print("Average Data", data.mean(), "", sep="\n")
    print("Max Data", data.max(), "", sep="\n")
    print("Min Data", data.min(), "", sep="\n")
